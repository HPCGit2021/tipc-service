export const folder = {
    f_recv: '/tesroot/comdata/TWTIPC/out',
    f_error: '/tesroot/tipc/error',
    f_resp: '/tesroot/tipc/resp',
    f_send: '/tesroot/comdata/KHHTIPC/in',
}

export let pathApi: any = {
    UpdateContainerYardStat: 'https://khnotify.twport.com.tw/updateContainerYardStat ',
    PreAdviceQuery: 'https://khnotify.twport.com.tw/PreAdviceQuery',
    EIRInfo: 'https://khnotify.twport.com.tw/udpateEIRInfo',
}