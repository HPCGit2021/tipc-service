import { validate } from "class-validator"
import { updateContainerYardStatValid } from '../types/updateContainerYardStat-valid'
import { updateEIRInfoValid } from '../types/updateEIRInfo-valid'
import { preAdviceQueryValid } from '../types/preAdviceQuery-valid' 

const eventType = {
    "updateContainerYardStat": updateContainerYardStatValid,
    "updateEIRInfo": updateEIRInfoValid,
    "preAdviceQuery": preAdviceQueryValid,
}

export enum EVENT {
    updateContainerYardStat = 'updateContainerYardStat',
    updateEIRInfo = 'updateEIRInfo',
    preAdviceQuery = 'preAdviceQuery'
}

export enum STATUS {
    SUCCESS = 200,
    WARNING = 400,
    ERROR = 500,
    CREATED = 201,
    TOOMANY = 429,
}

export function checkJson(json: string, type: EVENT) {

    if (type in EVENT) {
        return valid(new eventType[type](json))
    } else {
        return { code: 500, messeage: { errors: 'Unknown EventID' } }
    }

}

function valid(post: any) {
    return validate(post).then(errors => { // errors is an array of validation errors
        if (errors.length > 0) {
            let store: any = {}
            getErrors(errors, store)
            return { code: 400, messeage: store }
        } else {
            return { code: 200, messeage: post }
        }
    })
}

function getErrors(errors: any, store: any) {
    let l = errors.length
    for (let i = 0; i < l; i++) {
        if (errors[i].children && errors[i].children.length > 0) {
            store[errors[i].property] = {}
            getErrors(errors[i].children, store[errors[i].property])
        }
        else store[errors[i].property] = errors[i].constraints
    }
}