import fs from 'fs'
import pathStr from 'path'
import _, { object } from 'underscore'
import winston from "winston"
import moment from 'moment'
import log from './logs'
import { folder, pathApi } from '../config/tipc_config'
import { checkJson, STATUS } from './valid'

let tokenLog: log = new log('Token')

const soap = require('soap')
const fsEx = require('fs-extra')

class Controller {

    private url: string
    private cntSucess: number
    private cntError: number
    private stop:boolean
    public resultLog: log
    //private SignatureKeyID: string
    public logger: log
    public error401:boolean

    constructor() {
        this.logger = new log('send_file_to_TIPC_from_ALGETA')
        this.url = `${process.env['HOST']}`
        this.error401 = false
        this.cntSucess = 0
        this.cntError = 0
        this.stop = false
        this.resultLog = new log('receive_ALGETA_to_TIPC')
    }

    lastProcess(){
        let str = `\n[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] ---------- Last time result - Success : [${this.cntSucess}], Error : [${this.cntError}]\n`
        this.cntError = 0
        this.cntSucess = 0
        this.resultLog.writeLog(str)
        return this.stop
    }

    async getToken():Promise<string> {
        return soap.createClientAsync(this.url).then((client: any) => {
            return client.AuthenticateAsync({
                User_Login1d: process.env['USER'],
                User_Paxw0rd: process.env['PASS']
            }).then((result: any) => {
                //return this.convertJSONtoXML(result[0].return.SignatureKeyID)
                return result[0].return.SignatureKeyID
            })
        })
    }

    async countFiles(){
        let path = folder.f_recv
        // Sort list
        let files = fs.readdirSync(path);
        await files.sort(function (a, b) {
            return fs.statSync(pathStr.join(path, a)).mtimeMs - fs.statSync(pathStr.join(path, b)).mtimeMs
        });
        // Get 30 oldest files
        return _.toArray(files).slice(0, 30)
    }

    async sendFromOneFolder(token: string, list: string[]) {
        await Promise.all(
            list.map(file => {
                this.processSend(file, token)
            })  
        )  
    }

    async processSend(file: string, token: string) {
        let logInfo = `--------- Start send input JSON file: ${folder.f_recv}/${file} ---------\n`
        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Check validation data format \n`
        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Process send data JSON into TIPC \n`

        let readData = ''
        try {
            readData = fs.readFileSync(`${folder.f_recv}/${file}`, 'utf8')
            if (readData === "Stop") {
                return this.stop = true
            }
        } catch {
            logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: Can't read file \n`
            this.moveToErr(logInfo, file, "Can't read file")
        }

        let _this = this
        try {
            const data = JSON.parse(readData)
            // Valid json
            let checkData = await checkJson(data, data.EventID)
            if (checkData.code === STATUS.WARNING || checkData.code === STATUS.ERROR) {
                logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: ${JSON.stringify(checkData.messeage)} \n`
                this.moveToErr(logInfo, file, checkData.messeage)
            } else {
                //console.log(checkData.messeage)
                logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Check validation is OK \n`
                
                checkData.messeage.SignatureKeyID = token
                //console.log(checkData.messeage)
                switch(data.EventID) {
                    case 'updateContainerYardStat':
                        _this.WebService_UpdateContainerYardStat(_this.url, checkData.messeage, logInfo, file)
                        break
                    case 'updateEIRInfo':
                        _this.WebService_UpdateEIRInfo(_this.url, checkData.messeage, logInfo, file)
                        break
                    case 'preAdviceQuery':
                        let EWriteTime:string = data.EWriteTime
                        let SWriteTime:string = data.SWriteTime
                        if (EWriteTime === "" || EWriteTime === undefined) {
                            // add 10 minutes
                            let endDate = moment(SWriteTime, "YYYY-MM-DD HH:mm:ss").add(10, 'minutes')
                            checkData.messeage.EWriteTime = endDate.format("YYYY-MM-DD HH:mm:ss")
                            console.log(checkData.messeage)
                        }

                        let EndSiteID:string = data.EndSiteID
                        if (EndSiteID !== "" && EndSiteID !== undefined) {
                            let itemList:string[] = EndSiteID.split(",")
                            for (let i_item:number = 0; i_item < itemList.length; i_item++) {
                                let item:string = itemList[i_item]
                                checkData.messeage.EndSiteID = item
                                
                                _this.WebService_PreAdviceQuery(_this.url, checkData.messeage, logInfo)
                            }
                        } else {
                            checkData.messeage.EndSiteID = ""
                            _this.WebService_PreAdviceQuery(_this.url, checkData.messeage, logInfo)
                        }
                        
                        break
                }
            }
        } catch {
            logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: Can't parse to JSON data \n`
            this.moveToErr(logInfo, file, "Can't parse to JSON data")
        }        
    }

    WebService_PreAdviceQuery(url: string, data: any, logInfo:string) {
        return soap.createClientAsync(url).then((client: any) => {
            return client.PreAdviceQueryAsync(data).then((result: any) => {
                console.log(result[0].return.item)
                let date = moment().format('YYYYMMDDhhmmssSSS')
                result[0].return.item.map((e:any) => {
                    //this.wrireFile(logInfo, e, "preAdviceQuery")
                    //logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Response data into ${e} \n`
                    fs.writeFileSync(`${folder.f_send}/preAdviceQuery.${e.PortSRC}.${e.DataSRC}.${e.WorkNo}.${date}`, JSON.stringify(e))
                    logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Response data into ${folder.f_send}/preAdviceQuery.${e.PortSRC}.${e.DataSRC}.${e.WorkNo}.${date} \n`
                })                
                
                logInfo += `--------- Finish preAdviceQuery API ---------\n`
                return this.logger.writeLog(logInfo)
                //return result[0].return
            })
        })
    }

    WebService_UpdateEIRInfo(url: string, data: any, logInfo:string, file:any) {
        let subName:string = data.ContainerNo
        return soap.createClientAsync(url).then((client: any) => {
            return client.updateEIRInfoAsync(data).then((result: any) => {                
                // console.log("----------data UpdateEIRInfo------------------")
                // console.log(result[0].return.Map)
                // console.log("----------data format to JSON------------------")
                
                let dataResult = "{\"" + result[0].return.Map.item[0].key + "\":\"" + result[0].return.Map.item[0].value + "\""
                dataResult = dataResult + ",\"" + result[0].return.Map.item[1].key + "\":\"" + result[0].return.Map.item[1].value + "\"}"
                
                // console.log(dataResult)
                
                if  (dataResult !== "" && dataResult !== undefined) {
                    this.deleteFile(logInfo, file, dataResult, "updateEIRInfo", subName)
                }
                
                return result[0].return
            }).catch((err:any) => {
                this.moveToErr(logInfo, file, err)
            })
        })
    }

    WebService_UpdateContainerYardStat(url: string, data: any, logInfo:string, file:any) {
        let subName:string = data.containerYard
        //console.log(subName)
        return soap.createClientAsync(url).then((client: any) => {
            return client.updateContainerYardStatAsync(data).then((result: any) => {    
                // console.log("----------data UpdateContainerYardStat------------------")
                // console.log(result[0].return.item)
                // console.log("----------JSON UpdateContainerYardStat------------------")
                let dataResult = JSON.stringify(result[0].return.item)
                // console.log(dataJson)
                if  (dataResult !== "" && dataResult !== undefined) {
                    this.deleteFile(logInfo, file, dataResult, "updateContainerYardStat", subName)
                }

                return result[0].return
            }).catch((err:any) => {
                this.moveToErr(logInfo, file, err)
            })
        })
    }

    moveToErr(logInfo: string, file: any, err:any){
        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: ${err} \n`
        this.cntError++        
        
        fsEx.move(`${folder.f_recv}/${file}`, `${folder.f_error}/${file}`, { overwrite: true }, (error: any) => {                
            // Move file to error folder
            logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ${file} moved to /tipc/error ! \n`
            logInfo += `--------- Finish : ${folder.f_recv}/${file} ---------\n`
            return this.logger.writeLog(logInfo)
        })
        
    }

    deleteFile(logInfo: string, file: any, dataResult:any, EventID:string, subName:string){
        //let respData = JSON.stringify(dataResult)
        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Response data ${dataResult} \n`        
        let date = moment().format('YYYYMMDDhhmmssSSS')
        fs.writeFileSync(`${folder.f_resp}/${EventID}.${subName}.${date}`, dataResult)
        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Response data into ${folder.f_resp}/${EventID}.${subName}.${date} \n`
        
        this.cntSucess++
        fs.unlink(`${folder.f_recv}/${file}`, (err) => {
            if (err) {
                
                logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: ${err} \n`
                logInfo += `--------- Finish : ${folder.f_recv}/${file} ---------\n`
                return this.logger.writeLog(logInfo)
            }
            
            // Move file to error folder
            logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - File has been deleted ! \n`
            logInfo += `--------- Finish : ${folder.f_recv}/${file} ---------\n`
            return this.logger.writeLog(logInfo)
        })
    }

    wrireFile(logInfo: string, dataResult:any, EventID:string){
        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Response data into ${dataResult} \n`
        let date = moment().format('YYYYMMDDhhmmssSSS')
        fs.writeFileSync(`${folder.f_send}/${EventID}.${dataResult.PortSRC}.${dataResult.DataSRC}.${dataResult.WorkNo}.${date}`, JSON.stringify(dataResult))
        logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Response data into ${folder.f_send}/${EventID}.${dataResult.PortSRC}.${dataResult.DataSRC}.${dataResult.WorkNo}.${date} \n`
        return this.logger.writeLog(logInfo)
    }
    
    async processPreAdviceService(ctx:any) {
        let data = ctx.request.body
        //console.log(data)
        let logInfo = `--------- Start preAdviceQuery API---------\n`        
        

        if (data !== undefined) {
            let _this = this
            let token = await _this.getToken()
            if (!token) {
                tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get the token has failed. Try with access token ...\n`)
            } else {
                tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get token has done. Start process ...\n`)
            }
            //console.log(token)
            try {
                data.EventID = "preAdviceQuery"
                //console.log(data)
                // Valid json
                logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Check validation data format \n`
                let checkData = await checkJson(data, data.EventID)
                if (checkData.code === STATUS.WARNING || checkData.code === STATUS.ERROR) {
                    logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ERROR: ${JSON.stringify(checkData.messeage)} \n`
                    //this.moveToErr(logInfo, file, checkData.messeage)
                } else {
                    //console.log(checkData.messeage)
                    logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Check validation is OK \n`
                    
                    checkData.messeage.SignatureKeyID = token
                    //console.log(checkData.messeage)
                    logInfo += `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Process send data JSON into TIPC \n`
                                         
                    let EWriteTime:string = data.EWriteTime
                    let SWriteTime:string = data.SWriteTime
                    if (EWriteTime === "" || EWriteTime === undefined) {
                        // add 10 minutes
                        let endDate = moment(SWriteTime, "YYYY-MM-DD HH:mm:ss").add(10, 'minutes')
                        checkData.messeage.EWriteTime = endDate.format("YYYY-MM-DD HH:mm:ss")
                    }

                    let EndSiteID:string = data.EndSiteID
                    if (EndSiteID !== "" && EndSiteID !== undefined) {
                        let itemList:string[] = EndSiteID.split(",")
                        for (let i_item:number = 0; i_item < itemList.length; i_item++) {
                            let item:string = itemList[i_item]
                            checkData.messeage.EndSiteID = item
                            
                            _this.WebService_PreAdviceQuery(_this.url, checkData.messeage, logInfo)
                        }
                    } else {
                        checkData.messeage.EndSiteID = ""
                        _this.WebService_PreAdviceQuery(_this.url, checkData.messeage, logInfo)
                    }                            
                    
                }

                ctx.status = 200
                ctx.body = {
                    success: true,
                    result: "The system sent the result file successful."
                }

                let logInfo1 = `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Success: The system sent the result file into ${folder.f_send} \n`
                this.logger.writeLog(logInfo1)
                return ctx
            } catch {
                ctx.status = 500
                ctx.body = {
                    error: true,
                    result: "Can't get JSON data !!!"
                }
                let logInfo1 = `[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Error: Can't get JSON data !!! \n`
                this.logger.writeLog(logInfo1)
            }  
        }
    }
}

export default Controller