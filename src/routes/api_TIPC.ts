import fs from 'fs'
import schedule from 'node-schedule'
import moment from 'moment'
import Controller from '../controllers/controller'
import {folder} from '../config/tipc_config'
import log from '../controllers/logs'
import Router from 'koa-router'


let tokenLog: log = new log('Token')
let tipcController:Controller = new Controller() 

export async function runProcess() {
    try {
        await fs.mkdirSync(folder.f_send, { recursive: true })
        await fs.mkdirSync(folder.f_error, { recursive: true })
        await fs.mkdirSync(folder.f_resp, { recursive: true })
        await fs.mkdirSync(folder.f_recv, { recursive: true })
    } catch {
        return console.log("Can't find or create folder [out, error, in]")
    }

    let signatureKeyID = await tipcController.getToken()
    console.log("signatureKeyID:" + signatureKeyID)
    if (!signatureKeyID) {
        tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get the token has failed. Try with access token ...\n`)
    } else {
        tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get token has done. Start process ...\n`)
    }

    const sendFile = schedule.scheduleJob('*/1 * * * *', async function () {
        // Get the token when it is expired
        if (tipcController.error401) {
            tokenLog.writeLog(`\n[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get the token when it is expired ...\n`)

            signatureKeyID = await tipcController.getToken()
            if (!signatureKeyID) {
                tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get the token has failed. Try with access token ...\n`)
            } else {
                tipcController.error401 = false
                tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - Get token has done. Start process ...\n`)
            }
        }

        // Stop schedule
        if(tipcController.lastProcess()) {
            console.log(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ******* Stop schedule *******`)
            tokenLog.writeLog(`[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] - ******* Stop schedule ******* \n`)
            sendFile.cancel()
        }

        let list = await tipcController.countFiles()
        let len = list.length
        if (len > 0) {
            tipcController.logger.writeLog(`\n[${moment().format('YYYY-MM-DD HH:mm:ss.SSS')}] ---------- DIR_LOOP read_cnt: : [${len}]\n`);
            await tipcController.sendFromOneFolder(signatureKeyID, list)
        } else {
            console.log("No files to process ... ")
        }
    })
}


const router = new Router()
//POST preAdviceService API
router.post('/KHH/preAdviceService/requests/partner/:partnerId', async (ctx) => {
    const { partnerId } = ctx.params
    console.log(`partnerId : ${partnerId}`)
    
    await tipcController.processPreAdviceService(ctx)
    
  })


export default router