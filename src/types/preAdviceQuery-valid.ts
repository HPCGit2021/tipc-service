import {
    IsEnum,
    IsOptional,
    IsISO8601,
    IsString,
    IsNotEmpty,
} from 'class-validator'

import 'reflect-metadata'

export class preAdviceQueryValid {
    
    @IsOptional()
    @IsString()
    SignatureKeyID: string

    @IsOptional()
    @IsString()
    EndSiteID: string

    @IsNotEmpty()
    @IsString()
    SWriteTime: string

    @IsOptional()
    @IsString()
    EWriteTime: string

    constructor(json: any) {
        this.SignatureKeyID = json.SignatureKeyID
        this.EndSiteID = json.EndSiteID
        this.SWriteTime = json.SWriteTime
        this.EWriteTime = json.EWriteTime
    }
}