import {
    IsEnum,
    IsOptional,
    IsISO8601,
    IsString,
    IsNotEmpty,
} from 'class-validator'

import 'reflect-metadata'

export class updateEIRInfoValid {
    @IsOptional()
    @IsString()
    SignatureKeyID: string

    @IsNotEmpty()
    @IsString()
    ContainerNo: string // mandatory

    @IsNotEmpty()
    @IsString()
    OPClass: string // mandatory

    @IsOptional()
    @IsString()
    WorkNo: string

    @IsNotEmpty()
    @IsString()
    WareClass: string // mandatory

    @IsOptional()
    @IsString()
    Seal: string

    @IsOptional()
    @IsString()
    ContainerWeight: string

    @IsNotEmpty()
    @IsString()
    ShipName: string // mandatory

    @IsOptional()
    @IsString()
    CustomsRegNo: string

    @IsOptional()
    @IsString()
    SO: string

    @IsOptional()
    @IsString()
    Remark: string

    @IsOptional()
    @IsString()
    StartSiteID: string

    @IsOptional()
    @IsString()
    EndSiteID: string

    @IsNotEmpty()
    @IsString()
    ExpireTime: string // mandatory

    @IsOptional()
    @IsString()
    FinishTime: string

    constructor(json: any) {
        this.SignatureKeyID = 'updateEIRInfo'
        this.ContainerNo = json.ContainerNo
        this.OPClass = json.OPClass
        this.WorkNo = json.WorkNo
        this.WareClass = json.WareClass
        this.Seal = json.Seal
        this.ContainerWeight = json.ContainerWeight
        this.ShipName = json.ShipName
        this.CustomsRegNo = json.CustomsRegNo
        this.SO = json.SO
        this.Remark = json.Remark
        this.StartSiteID = json.StartSiteID
        this.EndSiteID = json.EndSiteID
        this.ExpireTime = json.ExpireTime
        this.FinishTime = json.FinishTime
    }
}