import {
    IsEnum,
    IsOptional,
    IsISO8601,
    IsString,
    IsNotEmpty,
} from 'class-validator'

import 'reflect-metadata'

export class updateContainerYardStatValid {
    @IsOptional()
    @IsString()
    SignatureKeyID: string

    @IsNotEmpty()
    @IsString()
    containerYard: string

    @IsNotEmpty()
    @IsString()
    pickup: string

    @IsNotEmpty()
    @IsString()
    return: string

    // ---------- Condition
    @IsNotEmpty()
    @IsString()
    totalnum: string

    @IsNotEmpty()
    @IsString()
    status: string

    @IsNotEmpty()
    @IsString()
    UpdateTime: string

    constructor(json: any) {
        this.SignatureKeyID = 'updateContainerYardStat'
        this.containerYard = json.containerYard
        this.pickup = json.pickup
        this.return = json.return
        this.totalnum = json.totalnum
        this.status = json.status
        this.UpdateTime = json.UpdateTime
    }
}