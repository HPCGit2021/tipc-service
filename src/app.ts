import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import dotenv from 'dotenv'
import fs from 'fs'
import json from 'koa-json'
if (fs.existsSync('.env')) {
    dotenv.config({ path: '.env' })
} else {
    dotenv.config({ path: '.env.dev' })  // you can delete this after you create your own .env file!
}

import { runProcess } from './routes/api_TIPC'
import router from './routes/api_TIPC'

const PORT = process.env['PORT']
const tipcApi = new Koa

tipcApi
    .use(bodyParser())
    .use(json())
    .use(router.routes())
    .use(router.allowedMethods())
    .listen(PORT, () => {
        console.log(`The server is running on ${PORT} !!! Press Ctrl + C to stop server !!!`)
        runProcess()
    })